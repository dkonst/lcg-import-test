xml = """<?xml version="1.0" encoding="UTF-8"?>
    <Site BuildName="{tag}-{platform}" BuildStamp="{datetime}-{tag}" Name="{hostname}" HostName="{hostname}" Generator="ctest-3.17.3" Append="true" OSName="Linux">
    <Testing>
        <StartDateTime>{sdate}</StartDateTime>
        <StartTestTime>{stimestamp}</StartTestTime>
        <TestList>
       """

xml2 = """
        </TestList>
       """

xml3 = """
        <EndDateTime>{edate}</EndDateTime>
        <EndTestTime>{etimestamp}</EndTestTime>
        <ElapsedMinutes>{etime}</ElapsedMinutes>
        </Testing>
    </Site>
"""

xmlt = """<Test Status="{status}">
            <Name>{name}</Name>
            <Path>{path}</Path>
            <FullName>{path}/{name}</FullName>
            <FullCommandLine>{command}</FullCommandLine>
            <Results>
                <NamedMeasurement type="numeric/double" name="Execution Time"><Value>0.0</Value></NamedMeasurement>
                <NamedMeasurement type="text/string" name="Completion Status"><Value>Completed</Value></NamedMeasurement>
                <NamedMeasurement type="text/string" name="Command Line"><Value>{command}</Value></NamedMeasurement>
                <Measurement>
                    <Value>{log}</Value>
                </Measurement>
            </Results>
            <Labels>
                <Label>Nightly</Label>
            </Labels>
        </Test>
"""
