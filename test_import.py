#!/usr/bin/env python
from __future__ import print_function
import importlib
import pkgutil
import sys
import argparse
import os
import traceback

blacklist = ["h5py.tests",  # requires testutil2
             "dateutil.tzwin", "lxml.usedoctest", "lxml.cssselect", "click._winconsole",
             "minrpc.windows", "prompt_toolkit.win32_types", "jpype._windows", "setuptools_scm.win_py31_compat",
             "send2trash.plat_win", "psutil._psaix", "psutil._psbsd", "psutil._psosx", "psutil._pssunos",
             "psutil._pswindows", "send2trash.plat_osx",
             "ipykernel._eventloop_macos", "PIL.FpxImagePlugin", "PIL.ImageGrab", "PIL.MicImagePlugin", "PIL.ImageCms", 
             "multiprocess.popen_spawn_win32", "paramiko._winapi", "paramiko.win_pageant", "serial.serialcli", 
             "serial.serialjava", "serial.serialwin32", "serial.win32", "tzlocal.win32", "tzlocal.windows_tz",
             "send2trash.plat_gio",  # requires PyGObject
             "prometheus_client.twisted",  # requires `twisted` - a network library
             "zmq.green",  # requires gevent (greenlet)
             "isort.pylama_isort",  # requires pylama for (optional) code linting
             "IPython.consoleapp", "IPython.kernel",  # deprecated; or add dependency on zeromq (pyzmq) to ipython
             "tensorflow.libtensorflow_framework",
             # probably not meant to be imported? "dynamic module does not define init function (initlibtensorflow_framework)"
             "lxml.usedoctest",  # not supposed to be imported directly, used by internal tests
             "storm.testing",  # requires pytest
             "storm.zope",  # requires zope
             "uncertainties.1to2",
             # not supposed to be imported: tool to convert from old (1.x) syntax to new (2.x) one
             "tornado.curl_httpclient",  # requires pycurl
             "sympy.galgebra",
             # FAIL: As of SymPy 1.0 the galgebra module is maintained separately at https://github.com/brombo/galgebra
             "rootpy.root2hdf5",  # requires PyTables
             "py2neo.console",  # depends on neo4j ("graph database")
             "pbr.tests",  # depends on testscenarios package
             "pbr.builddoc", "pbr.sphinxext",  # depends on sphinx
             "qm.setup",  # not supposed to be imported directly
             "pyarrow.cuda", "pyarrow.flight", "pyarrow.orc", "pyarrow.plasma",  # not built by default
             "dask.array",  # optional, requires: numpy, toolz
             "dask.dataframe",  # optional, requires: locket, toolz, partd, numpy, cloudpickle, pytz, six, python-dateutil, pandas, dask
             "distributed.joblib",  # It is no longer necessary to `import dask_ml.joblib` or `import distributed.joblib`.
             "qtpy.QtCharts", "qtpy.QtWebEngineWidgets",  # optional, not built by default. QtWebEngine is a huge pain to build.
             "distributed._ipython_utils",  # not supposed to be imported directly
             "distributed.asyncio", # deprecated
             "distributed.bokeh", # deprecated
             "partd.numpy", # requires numpy (optional)
             "partd.pandas", # requires pandas (optional)
             "partd.zmq", # requires zmq (optional)
             "pycparser._build_tables",  # not supposed to be imported directly
             "pygments.sphinxext",  # requires docutils
             "pyspark.shell",  # needs additional setup for Java
             "sympy.this",  # print "The Zen of Sympy" - just an easter egg
             "yaml.cyaml",  # cyaml is a wrapper around libyaml and is optional
             "cloudpickle.cloudpickle_fast",  # Requires Python 3.8+
             "simplejson.ordered_dict",  # Not to be imported directly - a replacement for OrderedDict if it's missing (Python <= 2.6)
             "matplotlib.pylab",  # "usage is not recommended"
             "matplotlib.tests", # test data is not installed
             "rpy2.interactive", # see https://bitbucket.org/rpy2/rpy2/issues/542/import-rpy2interactive-as-r-fails-on-rpy2
             "test.autotest",  # crashes
             # FAIL   It is no longer necessary to `import dask_ml.joblib` or `import distributed.joblib`.
             "pyarrow.orc", "pyarrow.plasma",  # not built
             "qtpy.QtCharts", "qtpy.QtWebEngineWidgets",  # underlying Qt modules not built
             "pycparser._build_tables",  # FAIL  [Errno 2] No such file or directory: '_c_ast.cfg'
             "plotly.config",  "plotly.dashboard_objs", "plotly.grid_objs", "plotly.plotly", "plotly.presentation_objs",  
             "plotly.session", "plotly.widgets",  # Deprecated
             "colorcet.plotting", # demo, requires numpy and holoview
             "jupyterlab.labhubapp",  # requires jupyterhub
             "pandas.conftest",  # FAIL  module 'hypothesis' has no attribute 'unlimited' - maybe update hypothesis?
             "pytools.mpiwrap",  # requires mpi4py
             "pytools.log", # requires logpyle
             "Cython.Coverage",  # code coverage test
             "numpy.conftest",  # requires `pytest`,
             "pathos.mp_map",  # blocks python
             "defusedxml.lxml",  # example code, requires lxml
             "jinja2.asyncfilters",  # inner circular dependency due to how we import the package, harmless https://github.com/pallets/jinja/issues/1144
             "tqdm.keras", # requires a dependency to numpy
             "tqdm.rich",  # requires rich module
             "awkward.libawkward-cpu-kernels",  # not a module
             "awkward.libawkward",  # not a module
             "onnxruntime.backend",  # requires onnx
             "onnxruntime.quantization",  # requires onnx
             "fsspec.fuse", # requires PyFuse
             "osgeo.utils", # deprecated in favour of osgeo_utils
            ]

blacklist2 = [
    "pexpect._async",  # Python 3.x only
    "couchdb.util3",  # only for python3
    "jinja2.asyncfilters",  # only for python3
    "jinja2.asyncsupport",  # only for python3
    "gast.ast3",  # only for python3
    "setuptools.lib2to3_ex",  # only for python3
    "distributed.asyncio",  # only for python3
    "jpype.imports",  # only for python3
    "cloudpickle.cloudpickle_fast",  # only for python3
    "importlib_resources._py3",
    "gast.ast3",
    "numba.unicode",  # should be fixed with an update
    "pyarrow.flight",
    "setuptools._imp",
    "pyarrow.dataset",  # only for python3
]

blacklist3 = [
    "couchdb.util2",  # only for python2
    "importlib_resources._py2", 
    "gast.ast2", 
    "minrpc.file_monitor",
]

blacklist4 = [
    "zmq.ssh",  # for MacOS
    "tensorboard.data",
    "tensorboard.default",
    "tensorboard.main",
    "tensorboard.program",
    "asn1crypto._elliptic_curve",
    "pyHepMC3.rootIO",
    "psutil._pslinux",
    "cpymad.libmadx"
]


blacklist39 = [
  "webargs.bottleparser",
  "webargs.falconparser",
  "webargs.flaskparser",
  "webargs.pyramidparser",
  "webargs.testing",
  "aiohttp.worker",
  "httpstan.openapi",
]

# add non-default folder for given package
# environment variable (ITK_CORE__HOME) comes from lcgenv_py
RUNTIME_ENV ={'itk': {'sys.path': ['$ITK_CORE__HOME/lib/python%s.%s/site-packages/itk' %
                                         (sys.version_info.major,sys.version_info.minor)]}
             }


def test_import(name, quiet):
    f = open("{0}.log".format(name), "w")
    print("Loading main module", name, file=f)
    f.flush()

    # prepare special runtime environment if necessary
    for variable, folders in RUNTIME_ENV.get(name, {}).items():
        pthsep = os.path.pathsep  # e.g ":" on linux
        joinedFolders = os.path.expandvars(pthsep.join(folders))
        if variable == 'sys.path':
            sys.path.append(joinedFolders)
        else:
            os.environ[variable] += pthsep + joinedFolders

    try:
        package = importlib.import_module(name)
    except BaseException as e:
        print(name + ": FAIL ", e, file=f)
        print(name + ": FAIL ", e)
        return False

    prefix = package.__name__ + "."
    failed = False
    if not hasattr(package, '__path__'):
        exit(0)

    for importer, modname, ispkg in pkgutil.iter_modules(package.__path__, prefix):
        if not quiet:
            print(name, modname + ": ", end=' ', file=f)
            f.flush()
        if modname.endswith("__main__") or modname in blacklist:
            if not quiet:
                print("SKIP BLACKLIST", file=f)
            continue

        try:
            importlib.import_module(modname)
        except Exception as e:
            if quiet:
                print(name, modname + ": ", end=' ', file=f)

            print("FAIL ", e, file=f)
            print(name, modname + ": FAIL ", e)
            traceback.print_exc()
            failed = True
        else:
            if not quiet:
                print("OK", file=f)

    return not failed


if __name__ == "__main__":
    if sys.version_info < (3, 0):
        blacklist.extend(blacklist2)
    else:
        blacklist.extend(blacklist3)
    if sys.platform == 'darwin':
        blacklist.extend(blacklist4)
        os.environ['DYLD_LIBRARY_PATH'] = os.environ['LD_LIBRARY_PATH'] 
    if sys.version_info >= (3, 8):
        blacklist.extend(blacklist39)

    parser = argparse.ArgumentParser()
    parser.add_argument("-q", "--quiet", help="Only print failures", action="store_true", dest="quiet")
    parser.add_argument("package")

    args = parser.parse_args()
    if test_import(args.package, args.quiet):
        exit(0)
    else:
        exit(1)


