#!/usr/bin/env python
from __future__ import print_function

import argparse
import datetime
import glob
import logging
import os
import time
import platform as _platform
from os import chmod, listdir
from os.path import join, isdir
from subprocess import Popen
from xml.sax.saxutils import escape
import platform
import sys

from xml_templates import xml, xml2, xml3, xmlt

try:
    from colorlog import colorlog
except ImportError:
    colorlog = None

python_version = 'python3'

root = ""
rel_root = '/cvmfs/sft.cern.ch/lcg/releases'
dev_root = "/cvmfs/sft-nightlies.cern.ch/lcg/nightlies"

lcgenv = '/cvmfs/sft.cern.ch/lcg/releases/lcgenv/latest/lcgenv_py3'

ignored_packages = ['QMtest', 'pytools', 'pygraphics', 'pyanalysis', 'sip', 
                    'jupyter_latex_envs', 'pjlsa', 'torch', 'torchvision',
                    'itk_core', 'itk_io', 'itk_filtering', 'itk_numerics',
                    'itk_registration', 'itk_segmentation', 'itk_meshtopolydata']

ignored_packages_extra = {'slc6': ['keras'],
                          'centos7': [],
                          'python2': [],
                          'python3': ['hepdata_converter', 'hepdata_validator', 'hspy'],
                          'mac': ['qtpy', 'PyRDF', 'qtconsole', 'pyqt5', 'parsl']}

# sip: needs a simpler testing script: from PyQt5 import sip
# TODO: pyspark.shell - java.lang.ClassNotFoundException: org.apache.hadoop.conf.Configuration
# TODO: pjlsa - how to set path to LSA jars?
# > Importing distributed._ipython_utils FAIL:
#   pyzmq/17.1.0/x86_64-centos7-gcc8-opt/lib/python2.7/site-packages/zmq/backend/cython/utils.so:
#   undefined symbol: zmq_curve_public
# TODO: torch & torchvision needs a custom LD_LIBRARY_PATH

ignored_modules = {'future': ['winreg'], 'matplotlib': ['mpl_toolkits'], 'pyxml': ['_xmlplus'], 'jpype': ['jpypex'],
                   'keras': ['docs'], 'sip': ['PyQt5'], 'py2neo': ['test'], 'PyRDF': ['tests'], 'Python': ['__pycache__'],
                   'jinja2': ['asyncfilters']}

# _xmlplus: needs patching, uses reserved keyword `as` as a variable name
# jpypex: should not be imported directly - need to start JVM in advance

runtime_deps = {'keras': ['tensorflow'], 'pygdal': ['XercesC'], 'spark': ['numpy', 'py4j'], 'dask': ['numpy'],
                'rpy2': ['pandas', 'ipython'], 'tensorboard': ['tensorflow'], 'tensorflow_estimator': ['tensorflow'],
                'ipython': ['pyzmq'], 'pyjapc': ['numpy'], 'pywt': ['png'], 'hepdata_converter': ['yoda', 'ROOT'],
                'PyRDF': ['spark', 'ROOT'], 'plotly': ['ipywidgets', 'notebook', 'numpy', 'scipy'], 
                'colorcet': ['scipy'], 'ipydatawidgets': ['ipywidgets', 'numpy', 'scipy']}

tests = []
logger = None


def setup_logging(logfile, debug, color):
    global logger
    if colorlog is None:
        color = False

    # print("Setup logging (debug is %s)" % (debug,))
    logger = logging.getLogger("import_test")
    logger.propagate = False
    handler = logging.StreamHandler()
    if color:
        handler.setFormatter(
            colorlog.ColoredFormatter(
                '%(asctime)s.%(msecs)03d %(log_color)s[%(name)s:%(levelname)s]%(reset)s %(message)s',
                datefmt='%H:%M:%S'))
    else:
        handler.setFormatter(logging.Formatter(fmt="%(asctime)s.%(msecs)03d [%(name)s:%(levelname)s] %(message)s",
                                               datefmt='%H:%M:%S'))

    file_handler = logging.FileHandler(logfile, "w")
    file_handler.setFormatter(logging.Formatter(fmt="%(asctime)s.%(msecs)03d [%(name)s:%(levelname)s] %(message)s"))

    logger.addHandler(handler)
    logger.addHandler(file_handler)

    if not debug:
        logger.setLevel(logging.INFO)
    else:
        logger.info("Debug logging is ON")
        logger.setLevel(logging.DEBUG)


def execute(cmd):
    p = Popen(cmd, shell=True)
    p.wait()
    return p.returncode == 0


def load_release(platform):
    keys = ('NAME', 'HASH', 'VERSION', 'PATH', 'DEPENDS')
    packages = {}

    fname = join(root, 'LCG_externals_' + platform + '.txt')
    logger.info("Loading release from {0}".format(fname))
    with open(fname) as f:
        for line in f:
            line = [x.strip() for x in line.strip().split(';')]
            if len(line) < 3:
                continue

            
            if line[0] in ignored_packages:
                logger.debug('Skipping ' + line[0] + '-' + line[2])
                continue
            else:
                logger.debug('Adding ' + line[0] + '-' + line[2])

            while len(line) < len(keys):
                line.append('')

            package = dict(zip(keys, line))
            package['PLATFORM'] = platform
            packages[line[0] + '-' + line[2]] = package

    return packages


def test_package(package, quiet):
    global tests
    retcode = 0
    env_template = 'eval "`{0} -p {1} {{PLATFORM}} {{NAME}} {{VERSION}}`"\n'.format(lcgenv, join(root))

    for pkgname in listdir(package['SITEPACKAGES']):
        if isdir(join(package['SITEPACKAGES'], pkgname)):
            logger.debug("Checking package {0}-{1} version {2} module {3}".format(package['NAME'], 
                                                                                  package['HASH'], 
                                                                                  package['VERSION'], pkgname))

            if pkgname.find('.') != -1:
                logger.debug("{0}: SKIP PKG_INFO".format(pkgname))
                continue
            
            if 'pycache' in pkgname:
                logger.debug("{0}: SKIP PYCACHE".format(pkgname))
                continue

            if pkgname in ignored_modules.get(package['NAME'], []):
                logger.debug("{0}: SKIP BLACKLIST".format(pkgname))
                continue

            this_test = {'name': pkgname, 'path': './testpythonimport/{0}-{1}'.format(package['NAME'], package['VERSION']),
                         'command': [env_template.format(**package).strip()],
                         'log': ''}

            # print("Checking package", p, "module", pkgname)
            with open("test.sh", "w") as fd:
                fd.write("#!/bin/bash \n")
                fd.write(env_template.format(**package))
                for name in runtime_deps.get(package['NAME'], []):
                    dep = {'NAME': name, 'VERSION': '', 'PLATFORM': package['PLATFORM']}
                    this_test['command'].append(env_template.format(**dep).strip())
                    fd.write(env_template.format(**dep))

                this_test['command'].append('python test_import.py {0}'.format(pkgname))
                this_test['command'] = escape(' && '.join(this_test['command']))
                
                fd.write("set -e\n")
                if quiet:
                    fd.write("python -u test_import.py -q {0} 2>{0}.err\n".format(pkgname))
                else:
                    fd.write("python -u test_import.py {0} 2>{0}.err\n".format(pkgname))

            chmod("test.sh", 0o744)
            pd = Popen("./test.sh", shell=False)
            pd.wait()
            failed = pd.returncode != 0
            if failed:
                retcode = 1
                this_test['status'] = 'failed'
                logger.warn(package['NAME'] + "-" + package['VERSION'] + " module " + pkgname + ": FAIL")
            else:
                this_test['status'] = 'passed'
                logger.debug(package['NAME'] + "-" + package['VERSION'] + " module " + pkgname + ": PASS")

            if os.path.exists("{0}.log".format(pkgname)):
                this_test['log'] = escape("--- Standard Output ---\n" + open("{0}.log".format(pkgname)).read())

            if os.path.exists("{0}.err".format(pkgname)):
                this_test['log'] += escape("--- Standard Error ---\n" + open("{0}.err".format(pkgname)).read())

            this_test['log'] = this_test['log'] or 'Unspecified failure'

            if not failed:
                try:
                    os.unlink("{0}.log".format(pkgname))
                except OSError:
                    pass

                try:
                    os.unlink("{0}.err".format(pkgname))
                except OSError:
                    pass
            else:
                os.rename("test.sh", "test_{NAME}-{VERSION}.sh".format(**package))

            tests.append(this_test)

    return retcode


def main_lcg(args):
    global root
    root = join(rel_root, args.release)
    return test(args)


def main_dev(args):
    global root
    weekday = datetime.date.today().strftime("%a")

    root = join(dev_root, args.release, weekday)
    return test(args)


def main_custom(args):
    global root
    root = args.release
    return test(args)


def test(args):
    retcode = 0
    packages = load_release(args.platform)
    logger.debug("Loaded " + str(len(packages)) + " packages")
    for p in packages:
        package_dir = join(args.release, packages[p]['PATH'])
        sitepackages_g = join(package_dir, 'lib/*/site-packages')
        sitepackages = glob.glob(sitepackages_g)

        if len(sitepackages) != 1:
            continue

        sitepackages = sitepackages[0]
        packages[p]['SITEPACKAGES'] = sitepackages
        retcode += test_package(packages[p], args.quiet)

    return 1 if retcode > 0 else 0


def main():
    global xml2, package, python_version, lcgenv

    parser = argparse.ArgumentParser()
    parser.add_argument("-q", "--quiet", help="Only print failures", action="store_true", dest="quiet")
    parser.add_argument("release")
    parser.add_argument("platform")

    argz = parser.parse_args()

    if 'cuda' in argz.release:
        ignored_packages.extend(['tensorflow', 'tensorboard', 'tensorflow_estimator', 'pycuda', 'cupy', 'pyopencl', 'TensorRT', 'root_numpy', 'PyRDF', 'itk', 'itkwidgets'])
        lcgenv = '/cvmfs/sft.cern.ch/lcg/releases/lcgenv/latest/lcgenv_py3'
        python_version = 'python3'
    
    if 'nxcals' in argz.release:
        ignored_packages.extend(['hepdata_converter']) #Since we do not build generators for nxcals, hepdata_converter failes to import yoda
        lcgenv = '/cvmfs/sft.cern.ch/lcg/releases/lcgenv/latest/lcgenv_py3'
        python_version = 'python3'

    if 'python2' in argz.release:
        lcgenv = '/cvmfs/sft.cern.ch/lcg/releases/lcgenv/latest/lcgenv_py2'
        python_version = 'python2'

    ignored_packages.extend(ignored_packages_extra[python_version])
    if 'slc6' in argz.platform:
        ignored_packages.extend(ignored_packages_extra['slc6'])

    if 'centos7' in argz.platform:
        ignored_packages.extend(ignored_packages_extra['centos7'])

    if 'mac' in argz.platform:
        ignored_packages.extend(ignored_packages_extra['mac'])

    setup_logging("__python_test.log", not argz.quiet, True)

    if len(argz.release) < 2:
        logger.error("Invaid release:" + str(argz.release))
        exit(1)

    sdate = datetime.datetime.now()

    if argz.release.startswith("LCG_"):
        exit_code = main_lcg(argz)
    elif argz.release[0].isdigit() and argz.release[1].isdigit():
        argz.release = 'LCG_' + argz.release
        exit_code = main_lcg(argz)
    elif argz.release.startswith("dev"):
        exit_code = main_dev(argz)
    elif isdir(argz.release):
        exit_code = main_custom(argz)
    else:
        logger.error("Invaid release:", argz.release)
        exit_code = 1

    edate = datetime.datetime.now()
    delta = (edate - sdate).seconds / 60.
    xmlroot = xml.format(version=argz.release,
                         platform=argz.platform,
                         datetime=os.environ.get('CTEST_TIMESTAMP', sdate.strftime("%Y%m%d-%H%M")),
                         hostname=os.environ.get('BUILDHOSTNAME', _platform.node()),
                         tag=os.environ.get('MODE', argz.release),
                         sdate=sdate.strftime("%b %d %H:%M CEST"),
                         stimestamp=int(time.mktime(sdate.timetuple())))

    testlist = ""

    for this_test in tests:
        testlist += "<Test>{0}/{1}</Test>\n".format(this_test['path'], this_test['name'])
        xml2 += xmlt.format(**this_test)

    xmlroot = xmlroot + testlist.rstrip() + xml2 + xml3.format(edate=edate.strftime("%b %d %H:%M CEST"), etime=delta, etimestamp=int(time.mktime(edate.timetuple())))
    with open(join(os.environ.get('WORKSPACE', os.getcwd()), "Test.xml"), "w") as f:
        f.write(xmlroot)

    exit(exit_code)


if __name__ == '__main__':
    main()
